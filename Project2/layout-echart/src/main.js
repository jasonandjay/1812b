import { createApp } from 'vue'
import App from './App.vue'

// 通过createApp实例化
createApp(App).mount('#app')
