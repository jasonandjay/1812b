// 劫持data对象
function reactive(data){
    let proxy = new Proxy(data, handler);
    return proxy;
}

// 使用Proxy+Reflect劫持data
const handler = {
    get(target, key){
        let value = Reflect.get(target, key);
        // 收集依赖
        track(target, key);
        if (typeof value === 'object'){
            return reactive(value);
        }else{
            return Reflect.get(target, key);
        }
    },
    set(target, key, value){
        Reflect.set(target, key, value);
        // 触发依赖更新
        trigger(target, key, value);
    }
}

// 所有effect集合栈
const effectLists = [];
/**  所有依赖收集集合
 * targetWeakMap{
 *  target: Map{
 *          key: Set<Effect>
 *      }
 * }
 */
const targetWeakMap = new WeakMap();

// 依赖收集函数
function track(target, key){
    let targetMap = targetWeakMap.get(target);
    if (targetMap === undefined){
        targetMap = new Map();
        targetWeakMap.set(target, targetMap);
    }
    let keySet = targetMap.get(key);
    if (keySet === undefined){
        keySet = new Set();
        targetMap.set(key, keySet);
    }
    const effect = effectLists[effectLists.length - 1];
    if (effect && !keySet.has(effect)){
        keySet.add(effect);
    }
}

// 依赖更新函数
function trigger(target, key){
    let targetMap = targetWeakMap.get(target);
    if (targetMap === undefined){
        return;
    }
    let keySet = targetMap.get(key);
    if (keySet === undefined){
        return;
    }
    keySet.forEach(effect=>effect())
}

// 计算属性
function computed(fn){
    const ef = effect(fn, {lazy: true});
    return {
        effect: ef,
        get value(){
            return ef();
        }
    }
}


// 收集副作用
function effect(fn, options={}){
    const rFn = createReactiveEffect(fn, options);
    if (!options.lazy){ // 通过lazy区分监听的是computed还是watch，computed延迟执行
        rFn();
    }
    return rFn;
}

// 创建响应式effect
function createReactiveEffect(fn ,options){
    function reactiveEffect(...args){
        return run(reactiveEffect, fn, args)
    }

    return reactiveEffect;
}

// 执行副作用
function run(reactiveEffect, fn, args){
    if (!effectLists.includes(reactiveEffect)){
        try{
            effectLists.push(reactiveEffect);
            return fn(...args);
        }finally{
            effectLists.pop();
        }
    }
}