import Compile from './compile.js';
import Observer from './observer.js';

class MVVM{
    constructor(options){
        this.$options = options;
        this.$data = options.data || {};
        // 对data做个代理
        this._proxy(this.$data);
        // 对computed做个代理
        this._proxy(options.computed);

        // 对$data做劫持
        new Observer(this, this.$data);
        // 编译模版
        new Compile(this.$options.el, this);
    }
    _proxy(data){
        for (let key in data){
            Object.defineProperty(this, key, {
                get(){
                    return data[key];
                },
                set(newVal){
                    data[key] = newVal;
                }
            });
        }
    }
}

export default MVVM;