import Dep from './dep.js';

class Watch{
    constructor(vm, exp, cb){
        this.vm = vm;
        this.exp = exp;
        this.cb = cb;
        this.get();
    }
    get(){
        if (!Dep.target){
            Dep.target = this;
        }
        this.getVal();
        Dep.target = null;
    }
    getVal(){
        console.log('exp...', this.exp);
        return this.exp.split('.').reduce((val, key)=>{
            return val[key];
        }, this.vm);
    }
    update(){
        this.cb(this.getVal());
    }
}

export default Watch;