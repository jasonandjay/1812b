import Dep from './dep.js'

class Observer{
    constructor(vm, data){
        this.dep = new Dep();
        this.vm = vm;
        this.proxy(data);
    }

    proxy(data){
        for (let key in data){
            if (typeof data[key] === 'object' && data[key] !== null){
                this.proxy(data[key])
            }
            this.defineReactive(data, key, data[key]);
        }
    }

    defineReactive(obj, key, value){
        let me = this;
        Object.defineProperty(obj, key, {
            set(newVal){
                me.dep.notify();
                value = newVal;
            },
            get(){
                if (Dep.target){
                    me.dep.addSub(Dep.target);
                }
                return value;
            }
        })
    }
}

export default Observer;