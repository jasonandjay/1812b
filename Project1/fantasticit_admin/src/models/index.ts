// 引入mobx模块
import User from './modules/user'
import Comment from './modules/comment'
import Category from './modules/category'
import Tag from './modules/tag'
import Article from './modules/article'
import Mail from './modules/mail'

export default {
    user: new User(),
    comment: new Comment(),
    category: new Category(),
    tag: new Tag(),
    article: new Article(),
    mail: new Mail
}