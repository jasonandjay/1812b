import { getCategoryList, publishArticle } from "@/service";
import { ICategoryItem } from "@/types";
import { IArticleItem } from "@/types/article";
import { makeAutoObservable, runInAction } from "mobx"

class Article{
    constructor(){
        makeAutoObservable(this);
    }

    async publishArticle(data: IArticleItem){
        let result = await publishArticle(data);
        if (result.data){
            
        }
        return result;
    }
}

export default Article;