import React from 'react';
import { useLocation } from 'umi';
import {Layout} from 'antd';
import MyHeader from '@/components/MyHeader'
import MyMenu from '@/components/MyMenu';

const whiteList = ['/login', '/redirect', '/article/editor', '/article/ameditor'];
const DefaultLayout:React.FC = (props)=>{
    const location = useLocation();
    console.log('location....', location)
    if (whiteList.indexOf(location.pathname) !== -1){
        return <>{props.children}</>;
    }else{
        return <Layout>
            <MyMenu></MyMenu>
            <Layout>
                <MyHeader></MyHeader>
                {props.children}
            </Layout>
        </Layout>
    }
}
export default DefaultLayout;