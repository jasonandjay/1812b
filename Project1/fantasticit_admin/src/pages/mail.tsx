import useStore from '@/context/useStore';
import { IMailItem } from '@/types';
import { makeHtml } from '@/utils/markdown';
import { Form, Button, Input, Table, Modal, message } from 'antd'
import {observer} from 'mobx-react-lite'
import { Key, useEffect, useState } from 'react';


const Main = ()=>{
    const [form] = Form.useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [params, setParams] = useState({});
    const [showModal, setShowedModal] = useState(false);
    const [reply, setReply] = useState('');
    const [current, setCurrent] = useState<IMailItem>();

    const [selectedRowKeys, setSelectedRowKeys] = useState<Key []>([])


    // 获取邮件列表
    useEffect(() => {
        store.mail.getMailList(page, params);
    }, [page, params]);

    // 条件查询
    function submit(){
        let values = form.getFieldsValue();
        let params:{[key: string]: string} = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
        setPage(1);
    }
    
    // 表格选中操作
    function onSelectChange(selectedRowKeys: Key[], selectedRows: IMailItem[]){
        setSelectedRowKeys(selectedRowKeys);
    }

    // 回复邮件
    async function replyMail(){
        if (!reply){
            message.warn('请输入回复内容');
            return;
        }
        let result = await store.mail.replyMail({
            to: current?.from!,
            subject: `回复: ${current?.subject}`,
            html: makeHtml(reply)
        })
        if(result.message !== '邮件发送成功'){
            message.warn(result.message);
        }else{
            message.success(result.message);
        }
        setShowedModal(false);
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }

    const columns = [{
        title: '发件人',
        dataIndex: 'from'
    }, {
        title: '收件人',
        dataIndex: 'to'
    }, {
        title: '主题',
        dataIndex: 'subject'
    }, {
        title: '发送时间',
        dataIndex: 'createAt'
    }, {
        title: '操作',
        render: (item: IMailItem)=>{
            return <div>
                <Button onClick={()=>store.mail.deleteMail([item.id])}>删除</Button>
                <Button onClick={()=>{
                    setShowedModal(true);
                    setCurrent(item);
                }}>回复</Button>
            </div>
        }
    }]
    return <div>
        <Form
            form={form}
            onFinish={submit}
        >
            <Form.Item
                label="发件人"
                name="from"
            >
                <Input type="text" placeholder="请输入发件人"/>
            </Form.Item>
            <Form.Item
                label="收件人"
                name="to"
            >
                <Input type="text" placeholder="请输入收件人"/>
            </Form.Item>
            <Form.Item
                label="主题"
                name="subject"
            >
                <Input type="text" placeholder="请输入主题"/>
            </Form.Item>
            <Button htmlType="submit" type="primary">搜索</Button>
            <Button htmlType="reset">重置</Button>
        </Form>
        <p>
            {selectedRowKeys.length?<Button onClick={()=>store.mail.deleteMail(selectedRowKeys as string[])}>删除</Button>:null}
            <Button onClick={()=>{
                setPage(1);
                setParams({...params});
            }}>刷新</Button>
        </p>
        <Table 
            rowSelection={rowSelection}
            // loading={Boolean(store.mail.mailList.length)}
            columns={columns} 
            dataSource={store.mail.mailList} 
            rowKey="id" 
            pagination={{showSizeChanger:true}}
        />
        <Modal 
            visible={showModal}
            onCancel={()=>setShowedModal(false)}
            footer={<Button onClick={()=>replyMail()}>回复</Button>}
        >
            <Input.TextArea rows={10} style={{marginTop: '25px'}} placeholder="支持markdown，html和文本回复..." value={reply} onChange={e=>setReply(e.target.value)}></Input.TextArea>
        </Modal>
    </div>
}

export default observer(Main);