import React, { useState } from 'react';
import { SketchPicker, ColorResult } from 'react-color'
import styles from './index.less';

const ColorPick: React.FC = () => {
    const [color, setColor] = useState('');

    function changeColor(color: ColorResult) {
        console.log('color...', color);
        setColor(color.hex);
        document.body.style.setProperty('--bg', color.hex);
    }
    return <div className={styles.wrap}>
        <SketchPicker
            color={color}
            onChangeComplete={changeColor}
        />
    </div>
}


export default ColorPick;