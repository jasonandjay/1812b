import { IArticleItem } from '@/types';
import React, { useContext, useEffect, useImperativeHandle, useRef } from 'react';
import { useHistory } from 'umi';
import share from '../Share';
import IndexContext from '@/context/index';
import useWinSize from '@/hooks/useWinSize';


interface IProps{
    articleList?: IArticleItem[], 
    [key:string]: any
}
const ArticleList: React.FC<IProps> = props => {
    const {title} = useContext(IndexContext);
    const child = useRef(null);
    const size = useWinSize();

    const history = useHistory();
    function shareArticle(e: React.MouseEvent, item: IArticleItem){
        e.stopPropagation();
        e.preventDefault();
        share(item);
    }
    const dom = useRef(null);
    console.log('articleList render...', dom.current, title);
    if (!props.articleList){
        return null;
    }

    function goDetail(item: IArticleItem){
        window._hmt.push(['_trackEvent', '首页', '文章列表跳转详情', '文章id', item.id]);
        history.push(`/article/${item.id}`)
    }
    // 刷访问量
    // useEffect(()=>{
    //     setTimeout(()=>{
    //         [...document.querySelectorAll(".article-item")].forEach(item=>{
    //             item.click();
    //         })
    //         window.location.reload();
    //     }, 3000);
    // }, [])
    return <div ref={dom}>
        <div>
            <p>页面的尺寸:</p>
            <p>
                <span>宽：{size.width}</span>
                <span>高：{size.height}</span>
            </p>
        </div>
        <Child ref={child}></Child>
        <button onClick={()=>{
            (child.current! as HTMLInputElement)?.focus();
        }}>获取Child组件内部input框的焦点</button>
         <button onClick={()=>{
            (child.current! as HTMLInputElement)?.setValue('子组件内部的input');
        }}>设置Child组件内部input框的值为：子组件内部的input</button>
        {
        props.articleList.map(item => {
            return <div onClick={()=>goDetail(item)} key={item.id} className="article-item">
                <p>{item.title}</p>
                <p>
                    <span>{item.updateAt}</span>
                    {/* <span>{item.tags}</span> */}
                </p>
                <img src={item.cover} alt="" />
                <p>{item.summary}</p>
                <p>
                    <span>{item.likes}</span>
                    <span>{item.views}</span>
                    <span onClick={e => shareArticle(e, item)}>分享</span>
                </p>
            </div>
        })
    }</div>
}

export default ArticleList;

const Child = React.forwardRef<HTMLInputElement>((props, ref)=>{
    const inputEle = useRef<HTMLInputElement>(null);
    useImperativeHandle(ref,  ()=>{
            return {
                focus: ()=>inputEle.current?.focus(),
                setValue: (value:string)=>{
                    inputEle.current!.value=value
                }
            }
        }
    )

    return <div>
        <input type="text" ref={inputEle} placeholder="输入框"/>
    </div>  
})