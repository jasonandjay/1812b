import HightLight from '@/components/HighLight';
import ImageView from '@/components/ImageView';
import { IRootState } from '@/types';
import { fomatTime } from '@/utils';
import {message} from 'antd';
import React, { useEffect, useState } from 'react';
import {IRouteComponentProps, useDispatch, useSelector} from 'umi'

const ArticleDetail: React.FC<IRouteComponentProps<{id:string}>> = props=>{
    let id = props.match.params.id;
    console.log('id..', id);
    const dispatch = useDispatch();
    const {articleDetail, articleComment} = useSelector((state:IRootState)=>state.article)
    const [commentPage, setCommentPage] = useState(1);

    useEffect(()=>{
        dispatch({
            type: 'article/getArticleDetail',
            payload: id
        })
    }, [])

    useEffect(()=>{
        dispatch({
            type: 'article/getArticleComment',
            payload: {
                id,
                page: commentPage
            }
        })
    }, [commentPage]);
    if (!Object.keys(articleDetail).length){
        return null;
    }
    let needPay = null;
    if (articleDetail.totalAmount){
        needPay = <button onClick={async ()=>{
            let result = await fetch('http://127.0.0.1:7001/pay', {
                method: 'POST',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify({
                    id: articleDetail.id,
                    totalAmount: articleDetail.totalAmount,
                })
            }).then(res=>res.json());
            console.log('result...',result)
            if (result.data){
                window.location.href = result.data;
            }else{
                message.warning(result.message);
            }
        }}>立即支付:¥{articleDetail.totalAmount}</button>
    }
    return <div>
        <ImageView>
            {articleDetail.cover && <img src={articleDetail.cover} />}
            {needPay}
            <h1>{articleDetail.title}</h1>
            <p>
                发布于{fomatTime(articleDetail.publishAt!)}•阅读量{articleDetail.views} 
            </p>
            <HightLight>
                <div dangerouslySetInnerHTML={{__html: articleDetail.html!}}></div>
            </HightLight>
            <p>发布于{fomatTime(articleDetail.publishAt!)} | 版权信息：非商用-署名-自由转载</p>
        </ImageView>
    </div>
}

export default ArticleDetail;