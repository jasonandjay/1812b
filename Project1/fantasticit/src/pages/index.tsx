import ArticleList from '@/components/ArticleList';
import { IRootState } from '@/types';
import React, { useCallback, useLayoutEffect, useMemo, useReducer } from 'react';
import { useEffect, useState } from 'react';
import { useDispatch, useHistory, useSelector } from 'umi';
import styles from './index.less'; // 等于启用了css-module
import IndexContext from '@/context/index';
const classNames = require('classnames');

const WrapArticleList = React.memo(ArticleList);
export default function IndexPage() {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const history = useHistory();
  const article = useSelector((state:IRootState) => state.article);
  const [state, reducerDispatch] = useReducer((state, action) =>{
    switch(action.type){
      case '+': return {...state, num:state.num+1};break;
      case '-': return {...state, num:state.num-1};break;
      default: return state;
    }
  }, {num: 100}, initialState=>({num: initialState.num*10}));

  useEffect(()=>{
    dispatch({
      type: 'article/getRecommend'
    })
    // setInterval(()=>{
    //   setPage(page=>page+1);
    // }, 1000);
  }, []);
  useEffect(()=>{
    dispatch({
      type: 'article/getArticleList',
      payload: page
    })
  }, []);

  useLayoutEffect(()=>{
    // alert(`当前page的值是：`+page);
  }, [page]);

  return (
    <div>
      <IndexContext.Provider value={{title: 'Index Page'}}>
        <h1 className={classNames(styles.title, styles.page)}>{page}</h1>
        <h1 className="title">Page index</h1>
        <WrapArticleList 
          articleList={article.articleList} 
          object={useMemo(()=>({}), [])}
          callback={useCallback(()=>{}, [])}
          ></WrapArticleList>
          <button onClick={()=>reducerDispatch({
            type: '+',
          })}>+</button>
          <span>{state.num}</span>
          <button onClick={()=>reducerDispatch({
            type: '-'
          })}>-</button>
        {/* <WrapArticleList></WrapArticleList> */}
      </IndexContext.Provider>
    </div>
  );
}