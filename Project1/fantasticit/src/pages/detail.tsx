import React,{useState, useEffect} from 'react';

const Detail: React.FC = ()=>{
    let [count, setCount] = useState(100);

    // 模拟didMount
    useEffect(()=>{
        console.log('123');
    }, []);

    // 监听count，实现didUpdate的功能
    useEffect(()=>{
        console.log('123');
    }, [count]);

    // 实现willUnmount
    useEffect(()=>{
        console.log('123');
        return ()=>{
            console.log('will unMount')
        }
    }, []);


    return <div>
        <p>我是详情页</p>
        <button onClick={()=>setCount(count+1)}>+</button>
        <p>{count}</p>
        <button onClick={()=>setCount(count-1)}>-</button>
    </div>
}

export default Detail;