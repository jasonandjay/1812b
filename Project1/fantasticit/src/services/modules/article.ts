import { request } from 'umi';
// 获取文章列表
export function getArticleList(page: number, pageSize = 12, status = 'publish'){
    return request('/api/article', {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            status,      
        },
        // 表示请求体里面传递的数据
        data: {},
    })
}

// 获取推荐文章
export function getRecommend(id?:string){
    let queryString = '';
    id && (queryString = `?articleId=${id}`);
    return request(`/api/article/recommend${queryString}`);
}

// 获取文章详情
export function getArticleDetail(id: string){
    return request(`/api/article/${id}/views`, {
        method: 'POST'
    })
}

// 获取文章评论
export function getArticleComment(id: string, page=1, pageSize=6){
    return request(`/api/comment/host/${id}?page=${page}&pageSize=${pageSize}`)
}