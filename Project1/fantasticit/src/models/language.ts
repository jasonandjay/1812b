import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface LanguageModelState {
  locale: string;
  locales: {label: string, locale: string}[]
}

export interface LanguageModelType {
  namespace: 'language';
  state: LanguageModelState;
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<LanguageModelState>;
  };
}
const languages: {[key:string]: string} = {
    'en': 'en-US',
    'en-US': 'en-US',
    'zh-CN': 'zh-CN'
}
let locale = navigator.language;
if (locale){
    locale = languages[locale];
}
const LanguageModel: LanguageModelType = {
  namespace: 'language',
  state: {
    locale: locale,
    locales: [{
        label: '汉语',
        locale: 'zh-CN'
    }, {
        label: '英文',
        locale: 'en-US'
    }]
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default LanguageModel;