import { ArticleModelState } from "@/models/article";
import { LanguageModelState } from "@/models/language";

export interface IRootState{
    article: ArticleModelState,
    language: LanguageModelState
    loading: {
        global: boolean;
    }
}