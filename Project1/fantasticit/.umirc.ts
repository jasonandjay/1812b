import { defineConfig } from 'umi';
const px2rem = require('postcss-px2rem');

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  // 引入antd
  antd: {

  },
  // 引入dva
  dva: {
    immer: true,
    hmr: true,
  },
  // 引入国际化
  locale: {},
  // 响应式

  extraPostCSSPlugins: [px2rem({remUnit: 75})],
  scripts: ["//g.tbcdn.cn/mtb/lib-flexible/0.3.4/??flexible_css.js,flexible.js"],
  // 配置服务器上的子路径
  publicPath: process.env.NODE_ENV === 'production' ? '/1812B/chenmanjie/fantasticit/' : '/',
  // 配置服务器上的路由前缀
  base: process.env.NODE_ENV === 'production' ? '/1812B/chenmanjie/fantasticit' : '/',
  // 配置路由的按需加载
  dynamicImport: {
    loading: '@/components/Loading',
  },
  // 文件名加上hash后缀
  hash: true,
  // 加入百度统计
  analytics: {
    baidu: 'b7a0296bf19580a2cab8bb41ec0d0d4f'
  }
});
