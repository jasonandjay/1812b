const path = require('path');

module.exports = {
    output: {
        publicPath: path.resolve(__dirname,'')
    },
    devServer: {
        port: 9999,
        open: true,
        historyApiFallback: true
    }
}