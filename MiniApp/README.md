## 小程序页面
### 路由
- 只有一级路由
- 传参只能通过url传参

### 路由跳转
- push方式：wx.navigateTo({url: })
- replace方式：wx.redirectTo({url: })
- 回退：wx.navigateBack({url: })
- 切换tab页：wx.switchTab({})
- 重新加载：wx.reaLaunch()
- 也可以使用组件：navigate

### 小程序生命周期
- 整个应用的生命周期
  - onLaunch
  - onReady
  - onShow
  - onHide
- 页面的生命周期
  - onLoad：页面进栈
  - onReady：页面进栈
  - onShow：页面出现在前台
  - onHide：页面消失在前台
  - onUnload: 页面出栈
  - onShareMessage：配置页面分享
  - onPulldownRefresh：下拉刷新
  - onReachBottom: 上拉加载

## 小程序样式
- 响应式单位：rpx
```css
在屏幕宽度750：
100rpx = 100px;
1rem = 100px;

在屏幕宽度375:
100rpx = 50px;
1rem = 50px;

=>得出rpx和rem的关系
1rem = 100rpx;
```