import request from '@/utils/request';

// 获取城市数据
export function getCityList(){
    return request.get('http://47.102.145.189:8009/area/city?level=1');
}

// 获取房源数据
export function getHourse(id){
    return request.get('http://47.102.145.189:8009/area/map?id='+id);

}