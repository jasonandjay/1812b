import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger'
 
// 引入子模块
import user from './modules/user'
import index from './modules/index'
import brand from './modules/brand'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user,
        index,
        brand
    },
    plugins: [createLogger()]
});