import {getBanner} from '@/services'

const state = {
    banners: []
};

const getters = {
    
}

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async getBanner({commit}, payload){
        let result = await getBanner();
        console.log('result...', result);
        if (result.status === 200){
            commit('update', {
                banners: result.body
            }, {root: true})
            commit({
                type: 'update',
                banners: result.body
            })

            commit('home/update', {
                banners: result.body
            })
        }
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
}